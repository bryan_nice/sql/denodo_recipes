WITH stats AS (
SELECT
  database_name
  , name
  , field_name
  , average_size
  , min_value
  , max_value
  , rows_number
  , distinct_values
  , null_values
  , CAST(null_values AS DECIMAL (18, 2))/CAST(rows_number AS DECIMAL (18, 2)) AS missing_percentage
  , CAST(distinct_values AS DECIMAL (18, 2))/CAST(rows_number AS DECIMAL (18, 2)) AS distinct_percentage
FROM GET_VIEW_STATISTICS()
)
SELECT
  database_name AS subject_area
  , name AS entity_name
  , 'entity' AS profile_level
  , 'Number of columns' AS profile_name
  ,  count(*) AS result
FROM stats
GROUP BY
  database_name
  , name
UNION
SELECT DISTINCT
  database_name AS subject_area
  , name AS entity_name
  , 'entity' AS profile_level
  , 'Number of rows' AS profile_name
  ,  rows_number AS result
FROM stats
UNION
SELECT
  database_name AS subject_area
  , name AS entity_name
  , 'entity' AS profile_level
  , 'Missing cells' AS profile_name
  , SUM(null_values) AS result
FROM stats
GROUP BY
  database_name
  , name
UNION
SELECT
  database_name AS subject_area
  , name AS entity_name
  , 'entity' AS profile_level
  , 'Missing cells %' AS profile_name
  , CAST(SUM(null_values) AS DECIMAL (18, 2))/CAST(SUM(rows_number) AS DECIMAL (18, 2)) AS result
FROM stats
GROUP BY
  database_name
  , name
UNION
SELECT
  database_name AS subject_area
  , name AS entity_name
  , field_name AS profile_level
  , 'Average size' AS profile_name
  , average_size AS result
FROM stats
UNION
SELECT
  database_name AS subject_area
  , name AS entity_name
  , field_name AS profile_level
  , 'Min value' AS profile_name
  , min_value AS result
FROM stats
UNION
SELECT
  database_name AS subject_area
  , name AS entity_name
  , field_name AS profile_level
  , 'Max value' AS profile_name
  , max_value AS result
FROM stats
UNION
SELECT
  database_name AS subject_area
  , name AS entity_name
  , field_name AS profile_level
  , 'Distinct value' AS profile_name
  , distinct_values AS result
FROM stats
UNION
SELECT
  database_name AS subject_area
  , name AS entity_name
  , field_name AS profile_level
  , 'Null value' AS profile_name
  , null_values AS result
FROM stats
UNION
SELECT
  database_name AS subject_area
  , name AS entity_name
  , field_name AS profile_level
  , 'Missing %' AS profile_name
  , CAST(null_values AS DECIMAL (18, 2))/CAST(rows_number AS DECIMAL (18, 2)) AS result
FROM stats
UNION
SELECT
  database_name AS subject_area
  , name AS entity_name
  , field_name AS profile_level
  , 'Distinct %' AS profile_name
  , CAST(distinct_values AS DECIMAL (18, 2))/CAST(rows_number AS DECIMAL (18, 2)) AS result
FROM stats